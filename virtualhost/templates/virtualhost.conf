<VirtualHost *:80>
    #Basic setup
    ServerAdmin {{admin}}
    ServerName  {{hostname}}
    # redirect to https automatically       
    RewriteEngine on
    RewriteCond %{HTTPS} off
    RewriteCond %{REQUEST_URI} !^\/.well-known
    RewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI}
    RewriteCond %{SERVER_NAME} ={{hostname}}
    RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent]
    DocumentRoot {{ document_root }}
</VirtualHost>

<VirtualHost *:443 localhost:443>
    SSLEngine On
    SSLProtocol all -SSLv2 -SSLv3
    SSLVerifyClient none
    ServerAdmin {{admin}}
    ServerName {{hostname}}
    DocumentRoot {{ document_root }}
    AcceptPathInfo  on
    RewriteEngine on
    LogLevel alert
    ErrorLog ${APACHE_LOG_DIR}/{{hostname}}.error.log
    CustomLog ${APACHE_LOG_DIR}/{{hostname}}.access.log combined
    IncludeOptional sites-available/{{hostname}}/*.conf
    SSLCertificateFile /etc/letsencrypt/live/{{hostname}}/fullchain.pem
    SSLCertificateKeyFile /etc/letsencrypt/live/{{hostname}}/privkey.pem
    Include /etc/letsencrypt/options-ssl-apache.conf
    IncludeOptional sites-available/{{hostname}}/*.conf
    {% for alias in aliases %}
    Alias {{alias.target}} {{alias.source}}
        <Directory {{alias.source}}>
        #Order Allow,Deny
        Allow from all
        # Don't show indexes for directories
        Options -Indexes
        </Directory>
    {% endfor %} 
</VirtualHost>