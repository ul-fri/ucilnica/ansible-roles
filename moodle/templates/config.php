<?php  // Moodle configuration file

unset($CFG);
global $CFG;
$CFG = new stdClass();

$CFG->dbtype    = 'pgsql';
$CFG->dblibrary = 'native';
$CFG->dbhost    = 'localhost';
$CFG->dbname    = '{{moodle_dbname}}';
$CFG->dbuser    = '{{moodle_dbuser}}';
$CFG->dbpass    = '{{moodle_dbpass}}';
$CFG->prefix    = 'mdl_';
$CFG->dboptions = array (
  'dbpersist' => 0,
  'dbport' => '',
  'dbsocket' => '',
);

$CFG->wwwroot   = '{{moodle_wwwroot}}';
$CFG->dataroot  = '{{moodle_dataroot}}';
$CFG->admin     = 'admin';

$CFG->directorypermissions = 0777;

$CFG->customfiletypes = array(
  (object)array(
      'extension' => 'py',
      'icon' => 'sourcecode',
      'type' => 'application/python',
      'customdescription' => 'Python script'
  )
);

require_once(dirname(__FILE__) . '/lib/setup.php');

// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!